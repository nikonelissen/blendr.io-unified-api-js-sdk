# Blendr.io Javascript SDK  

## Installation

### Browser

```html
<script src="blendr-io-unifiedapi.js"></script>
```
See blendr-example.html for more info

### NodeJS
```bash
npm install blendr.io-unified-api-js-sdk
```
See nodejs-example.js for more info

## Usage

Create a new instance using your credentials

```javascript
var unifiedApi = UnifiedAPI(appid, apikey);
```

Making an API call
```javascript
unifiedApi.hubspotListContacts(callback);
function callback(err, result) {
    if(!err) {
        handleResponse(result);
    } else {
        console.log(err);
    }
}
```

Making an API call with parameters
```javascript
let params = {q: 'test'}
unifiedApi.hubspotSearchContacts(callback, params);
function callback(err, result) {
    if(!err) {
        handleResponse(result);
    } else {
        console.log(err);
    }
}
```