var UnifiedAPI = require("../src/blendr-io-unifiedapi");
var test = require('unit.js');
require('dotenv').config();

// Setup
var unifiedAPI = UnifiedAPI(process.env.APPID, process.env.APIKEY);

// Test wrapper
describe('Testing wrapper functions', function () {
    it('hubspot list contacts', function () {
        unifiedAPI.hubspotListContacts(function (err, result) {
            test.assert(err === null);
            test.assert(result[0]["name"] !== null);
            test.string(result[0]["name"]);
        });
    });
    it('salesforce list contacts', function () {
        unifiedAPI.salesforceListContacts(function (err, result) {
            test.assert(err === null);
            test.assert(result[0]["name"] !== null);
            test.string(result[0]["name"]);
        });
    });
});


