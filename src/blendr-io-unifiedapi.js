"use strict";
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
    var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

(function () {
    var UnifiedAPI = (function () {
        var UnifiedAPI = function (appid, apikey) {
            return new Proxy({}, {
                apiUrl: 'https://admin.blendr.io',
                appid: appid,
                apikey: apikey,
                get: function (target, name) {
                    let methodNameParts = name.split(/(?=[A-Z])/);
                    if (methodNameParts.length !== 3) {
                        throw "Method name should be camel case, format: datasourcetypeActionObjectType(), e.g. twitterListFollowers()";
                    }
                    let datasourcetype = methodNameParts[0].toLowerCase();
                    let action = methodNameParts[1].toLowerCase();
                    let objecttype = methodNameParts[2].toLowerCase();

                    let datatype = "objects";
                    if (action === "getmetric") {
                        action = "get";
                        datatype = "metrics";
                    }

                    let queryString = '';
                    if (action === 'list' || action === 'search' || action === 'get') {
                        queryString = 'fields=*'; //request all fields
                    }

                    let url = this.apiUrl + "/api/v1/" + datatype + "/" + encodeURI(datasourcetype) + "/" + objecttype + "/" + action + "?" + queryString;
                    let xhr = new XMLHttpRequest();
                    let that = this;
                    return function (cb, params) {

                        if (params !== undefined) { // Not every API call requires parameters
                            for (let i in params) {
                                console.log(params);
                                url += "&" + i + "=" + params[i];
                            }
                        }

                        xhr.open("POST", url, true);
                        xhr.setRequestHeader('appid', that.appid);
                        xhr.setRequestHeader('apikey', that.apikey);
                        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

                        //Send the proper header information along with the request
                        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState == 4) {
                                if (xhr.status == 200) {
                                    cb(null, xhr.responseText);
                                } else {
                                    cb(xhr, null);
                                }
                            }
                        };
                        xhr.send();
                    }
                }
            });
        };
        return UnifiedAPI;
    })();

    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
        module.exports = UnifiedAPI;
    else
        window.UnifiedAPI = UnifiedAPI;
})();
