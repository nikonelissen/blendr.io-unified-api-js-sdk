var UnifiedAPI = require('./src/blendr-io-unifiedapi');
require('dotenv').config();

var unifiedAPI = UnifiedAPI(process.env.APPID, process.env.APIKEY);

unifiedAPI.hubspotListContacts(function(err, result) {
    if(!err) {
        console.log(result);
    } else {
        console.log(err);
    }
});